import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.commons.io.FileUtils;

import com.jmatio.io.MatFileReader;
import com.jmatio.types.MLArray;
import com.jmatio.types.MLCell;
import com.jmatio.types.MLChar;

/*used this class to copy various files from source to destination.*/
public class PutImagesInAFolder {
	private static void copyFile(File sourceFile, File destFile)
	        throws IOException {
	    if (!sourceFile.exists()) {
	        return;
	    }
	    if (!destFile.exists()) {
	        destFile.createNewFile();
	    }
	    FileChannel source = null;
	    FileChannel destination = null;
	    source = new FileInputStream(sourceFile).getChannel();
	    destination = new FileOutputStream(destFile).getChannel();
	    if (destination != null && source != null) {
	        destination.transferFrom(source, 0, source.size());
	    }
	    if (source != null) {
	        source.close();
	    }
	    if (destination != null) {
	        destination.close();
	    }

	}
	public static void main(String[] args) throws FileNotFoundException, IOException {
		// TODO Auto-generated method stub
		 MatFileReader matfilereader = new MatFileReader("/Users/prateek/Documents/workspace/EyeTribeTest/filenamesVOC07.mat");
		// MLCell mlCell = (MLCell) matfilereader.getMLArray("classnames");
		 
		 MLCell mlCell = (MLCell) matfilereader.getMLArray("images");
		 MLCell mlArrayRetrieved = (MLCell)mlCell.get(19);
		 ArrayList <String> nameOfImages= new ArrayList<String>();
		 HashMap<String,  ArrayList <String>> allImages = new HashMap<String,  ArrayList <String>>();
		 for(int i=0; i<mlArrayRetrieved.getSize(); i++){
			 nameOfImages.add(mlArrayRetrieved.get(i).contentToString().substring(7, 13));
			//System.out.println(mlArrayRetrieved.get(i).contentToString());
		 }
		 allImages.put("aeroplane", nameOfImages);
		 for(String i: nameOfImages){
			 System.out.println(i);
			
		 String sour ="/Users/prateek/Documents/workspace/EyeTribeTest/VOCdevkit2/VOC2007/JPEGImages/"+ i +".jpg";
		 
		 File source = new File(sour);
		 File dest = new File("/Users/prateek/Documents/workspace/EyeTribeTest/TVMonitor");
		 FileUtils.copyFileToDirectory(source, dest);
		// copyFile(source, dest);
//		 try {
//		    // FileUtils.copyDirectory(source, dest);
//		     FileUtils.copyFile(source, dest);
//		 } catch (IOException e) {
//		     e.printStackTrace();
//		 }
		 }
	//	 MLChar j = (MLChar) matfilereader.getMLArray("classnames");
		// System.out.println(mlArrayRetrieved);
		 //System.out.println(mlArrayRetrieved.contentToString());
		 MLArray a = mlCell.get(3);
	
		// System.out.println(mlArrayRetrieved.get(0).contentToString());
	}

}
