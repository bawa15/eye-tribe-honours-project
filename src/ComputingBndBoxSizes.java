
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/*Class used to compute the area of bounding box
 * */
public class ComputingBndBoxSizes {

	 private static final String FILE_NAME = "/tmp/MyFirstExcel.xlsx";

	 public static void main(String[] args) throws IOException {
			// TODO Auto-generated method stub
		 File fileName = new File("/Users/prateek/Documents/workspace/EyeTribeTest/Experiments/Annotator2/Accuracies:Error/cow/fileNames.txt");
		 BufferedReader br = null;
		 br = new BufferedReader(new FileReader(fileName));
		 String line;
		 while((line = br.readLine()) != null){
			 File folder = new File("/Users/prateek/Documents/workspace/EyeTribeTest/VOCdevkit3/VOC2007/Annotations");
			 File[] files = folder.listFiles();
			 for(File fil : files){
				 if(fil.getName().substring(0, 6).equals(line)){
					// System.out.println(fil.getName());
					 int counter = 0;
					 try {

							File fXmlFile = fil;
							DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
							DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
							Document doc = dBuilder.parse(fXmlFile);

							//optional, but recommended
							//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
							doc.getDocumentElement().normalize();

							///System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

							NodeList nList = doc.getElementsByTagName("object");

							//System.out.println("----------------------------");

							for (int temp = 0; temp < nList.getLength(); temp++) {

								Node nNode = nList.item(temp);

								
								
								if (nNode.getNodeType() == Node.ELEMENT_NODE) {
									
									Element eElement = (Element) nNode;
									String object = eElement.getElementsByTagName("name").item(0).getTextContent();
									if(object.equals("cow")){
//										System.out.println("\nCurrent Element :" + nNode.getNodeName());
//										System.out.println("name : " + eElement.getElementsByTagName("name").item(0).getTextContent());
//										System.out.println("ymin : " + eElement.getElementsByTagName("ymin").item(0).getTextContent());
//										System.out.println("xmax : " + eElement.getElementsByTagName("xmax").item(0).getTextContent());
//										System.out.println("xmin : " + eElement.getElementsByTagName("xmin").item(0).getTextContent());
//										System.out.println("ymax : " + eElement.getElementsByTagName("ymax").item(0).getTextContent());
										int yMin = Integer.parseInt(eElement.getElementsByTagName("ymin").item(0).getTextContent());
										int yMax = Integer.parseInt(eElement.getElementsByTagName("ymax").item(0).getTextContent());
										int xMin = Integer.parseInt(eElement.getElementsByTagName("xmin").item(0).getTextContent());
										int xMax = Integer.parseInt(eElement.getElementsByTagName("xmax").item(0).getTextContent());
										int width = yMax - yMin;
										int height = xMax - xMin;
										int area = width*height;
										int sqrtArea = (int) Math.sqrt(area);
										
										counter += 1;
										if(counter == 1){
											System.out.println(area);
											//System.out.println(sqrtArea);
										}
										//System.out.println("Sqrt: "+sqrtArea);
										
									}
									//System.out.println("ymax : " + eElement.getAttribute("ymax"));
									

								}
							}
						    } catch (Exception e) {
							e.printStackTrace();
						    }
				 }
			 }
		 }
		 
		}
}
