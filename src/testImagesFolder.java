import java.awt.EventQueue;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import javax.swing.JDialog;
import javax.swing.JFileChooser;


/*Creates the file chooser. This is what the programme starts with*/
public class testImagesFolder {
	//return file (image) names in the chosen directory
	
	/*public void imageResize(File url) throws IOException{
		BufferedImage originalImage = ImageIO.read(url);
		int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
        
        BufferedImage resizeImageJpg = ImagingTest.resizeImage(originalImage, type, 1280, 800);
        //Thread.sleep(5);
        ImageIO.write(resizeImageJpg, "jpg", url);
	}*/
	private JFileChooser chooser;
	private int returnVal;
	public testImagesFolder() throws InvocationTargetException, InterruptedException{
		this.chooser = new JFileChooser();
		
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        //current directory is set
        
        chooser.setCurrentDirectory(new java.io.File("."));
        EventQueue.invokeAndWait(new Runnable() {
            //private JFileChooser chooser;
            
			@Override
            public void run() {
               // String folder = System.getProperty("user.dir");
                chooser = new JFileChooser();
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                chooser.setCurrentDirectory(new java.io.File("."));
                returnVal = chooser.showOpenDialog(null);
                System.out.println("rog="+returnVal);
            }
        });
        System.out.println(returnVal);
        //returnVal = chooser.showOpenDialog(null);
       
       
	}
    public ArrayList<String> getFilePaths() 
    {
    	String directory;
        //widget to let users select a directory or file
        //JFileChooser chooser = new JFileChooser();
        ArrayList<String> absoluteFilePaths = new ArrayList<String>();
        //holds all file (image) names in the chosen directory
        ArrayList<String> myArr = new ArrayList<String>();
        File[] listOfFiles;
        //only allow directory selection
       // chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        //current directory is set
        
       // chooser.setCurrentDirectory(new java.io.File("."));
        System.out.println("Current Directory="+chooser.getCurrentDirectory());
        
        
        //pops up file chooser dialog, user chooses a directory
       // int returnVal = chooser.showOpenDialog(null);
        
        //if the selected option was approved
        if(returnVal == JFileChooser.APPROVE_OPTION) 
        {
        	String fil = chooser.getSelectedFile()+"/Unknown1.png";
        //	String fullPath = file.getAbsolutePath();
        	//File file = new File(fil);
        	
        	System.out.println("Absolute Path="+fil);
//        	try {
//				imageResize(file);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
            //directory object
            File folder = chooser.getSelectedFile();
            //directory string
            directory = chooser.getSelectedFile() + "\\";
           
            //list files objects in the directory object
            listOfFiles = folder.listFiles();
         
            //put all the names of the file objects into myArr
            for (int i = 0; i < listOfFiles.length; i++) 
            {
                if (listOfFiles[i].isFile()) 
                {
                    //myArr.add(directory+listOfFiles[i].getName());
                 //   System.out.println("kist of file="+listOfFiles[i]);
                    String name = listOfFiles[i].getAbsolutePath();
                    String name1 = listOfFiles[i].getAbsolutePath();
               //     System.out.println("Name BAOVE="+name);
                    if(name.endsWith(".jpg") || name.endsWith(".png")){
            			System.out.println("name");
            			myArr.add(directory+listOfFiles[i].getName());
            			absoluteFilePaths.add(name1);
            		   // File imageToReSize = new File(name);
//            		    try {
//            				imageResize(listOfFiles[i]);
//            			} catch (IOException e) {
//            				// TODO Auto-generated catch block
//            				e.printStackTrace();
//            			}
            		}
                    
                }//end inner if
            }//end for         
        }//end outer if
        //else no selection was made
        else 
        {
            System.out.println("No Selection ");
            return null;
        }//end else

        return myArr;
    }//end method
    
    public ArrayList<String> getAbsoluteFilePaths() 
    {
    	//String directory;
        //widget to let users select a directory or file
       // JFileChooser chooser = new JFileChooser();
        
        //holds all file (image) names in the chosen directory
       // ArrayList<String> myArr = new ArrayList<String>();
        ArrayList<String> absoluteFilePaths = new ArrayList<String>();
        File[] listOfFiles;
        //only allow directory selection
     //   chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        //current directory is set
        
   //     chooser.setCurrentDirectory(new java.io.File("."));
        System.out.println("Current Directory="+chooser.getCurrentDirectory());
        
        
        //pops up file chooser dialog, user chooses a directory
       // int returnVal = chooser.showOpenDialog(null);
        
        //if the selected option was approved
        if(returnVal == JFileChooser.APPROVE_OPTION) 
        {
        	String fil = chooser.getSelectedFile()+"/Unknown1.png";
        //	String fullPath = file.getAbsolutePath();
        	//File file = new File(fil);
        	System.out.println("Absolute Path="+fil);
//        	try {
//				imageResize(file);
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
            //directory object
            File folder = chooser.getSelectedFile();
            //directory string
            //directory = chooser.getSelectedFile() + "\\";
            
            //list files objects in the directory object
            listOfFiles = folder.listFiles();
         
            //put all the names of the file objects into myArr
            for (int i = 0; i < listOfFiles.length; i++) 
            {
                if (listOfFiles[i].isFile()) 
                {
                    //myArr.add(directory+listOfFiles[i].getName());
                 //   System.out.println("kist of file="+listOfFiles[i]);
                    //String name = listOfFiles[i].getAbsolutePath() + listOfFiles[i];
                    String name1 = listOfFiles[i].getAbsolutePath();
               //     System.out.println("Name BAOVE="+name);
                    if(name1.endsWith(".jpg") || name1.endsWith(".png")){
            			System.out.println("name");
            			//absoluteFilePaths.add(name);
            			absoluteFilePaths.add(name1);
            		   // File imageToReSize = new File(name);
//            		    try {
//            				imageResize(listOfFiles[i]);
//            			} catch (IOException e) {
//            				// TODO Auto-generated catch block
//            				e.printStackTrace();
//            			}
            		}
                    
                }//end inner if
            }//end for         
        }//end outer if
        //else no selection was made
        else 
        {
            System.out.println("No Selection ");
            return null;
        }//end else

        return absoluteFilePaths;
    }//end method
  
    public ArrayList<String> getImageList(ArrayList<String> list){
    	//System.out.println("here"+list);
    	
    	ArrayList<String> trye = new ArrayList<String>();
    	//trye.addAll(list);
    	int index ;
    	String imageName;
    	for(String name : list){
    		if(name.endsWith(".jpg") || name.endsWith(".png")){
    			index = name.lastIndexOf("\\");
    			
        		imageName = name.substring(index +1);
        		
        		trye.add(imageName);
        		
    		}
    		
    	}
    								//	String fullPath = "C:\\Hello\\AnotherFolder\\The File Name.PDF";
	
		return trye;
    }
   
	/*public static void main(String[] args) {
		// TODO Auto-generated method stub
		testImagesFolder j = new testImagesFolder();
		//System.out.println(j.getFileNames());
		ArrayList<String> filePaths = new ArrayList<String>();
		
		filePaths = j.getFilePaths();
		
		
		for(String a : filePaths){
			System.out.println("Path=");
			System.out.println(a);
		}
		
		ArrayList<String> fileNames = new ArrayList<String>();
		fileNames = j.getImageList(filePaths);
//		fileNames = j.getImageList(filePaths);
//																//	String fullPath = "C:\\Hello\\AnotherFolder\\The File Name.PDF";
//																		//int index = fullPath.lastIndexOf("\\");
//																		//String fileName = fullPath.substring(index + 1);
//																	//System.out.println(fileName);
// 		for(String a : fileNames){
//// 			//fullPath = a;
//// 			//index = fullPath.lastIndexOf("\\");
//// 			//fileName = fullPath.substring(index + 1);
//// 			//System.out.println("File name extracted="+fileName);
// 			System.out.println("File Name=");
//			System.out.println(a);
// 		}
	}*/

}
