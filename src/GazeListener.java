import java.awt.AWTException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.PointerInfo;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.lang.reflect.InvocationTargetException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import com.theeyetribe.clientsdk.IGazeListener;
import com.theeyetribe.clientsdk.data.GazeData;


/*Class handling all functions to record data, handles key action listeners.
 * Makes all the required checks. Refactors heights/widths.*/
public class GazeListener extends justFrame implements IGazeListener, KeyListener, MouseListener{
	
	private double averageTime = 0;
	protected PrintWriter outputStream2;
	protected PrintWriter fixationsWriter;
	private ArrayList<String> testImagesPath;
	public boolean isFirstTime = true;
	public ArrayList<String> imageList = new ArrayList<String>();
	private justFrame j;
	public boolean once = false;
	public boolean trainingMode = false;
	private int imageIndex = 0;
	private long initialTimeStamp;
	private long finalTimeStamp;
	private int fixationCounter = 0;
	private PrintWriter startFromPreviousTime;
	private String previousImageIndexFName;
	private GazeData currentGazeData;
	private File previousImageIndex;
	private boolean firstTimeRunForTime = true;
	public GazeListener() throws IOException, InvocationTargetException, InterruptedException{
		
		this.j = new justFrame();
		
		showFrame();
		
		this.imageList = j.getImagesList();
	
		this.j.jF.addKeyListener(this);
		this.j.jF.addMouseListener(this);
	
		startFromPreviousTime = null;
		previousImageIndexFName = "PreviousImageIndex.txt";
		previousImageIndex = new File(previousImageIndexFName);
		this.testImagesPath = j.testImagesAbsoluteFilePaths;
		

		
	}
       
	public void showFrame() throws IOException, InvocationTargetException, InterruptedException{
		this.j.putImagesInArrayList();
		this.j.showFrame(imageIndex);
		
	
	}
	/*This function is for visualisation as well
	 * Can be used for other if needed. Basically checks if the file is written properly or not. 
	 * */
	@SuppressWarnings("unused")
	private boolean isCompletelyWritten(File file) {
	    RandomAccessFile stream = null;
	    try {
	        stream = new RandomAccessFile(file, "rw");
	        return true;
	    } catch (Exception e) {
	       // log.info("Skipping file " + file.getName() + " for this iteration due it's not completely written");
	    } finally {
	        if (stream != null) {
	            try {
	                stream.close();
	            } catch (IOException e) {
	               // log.error("Exception during closing file " + file.getName());
	            }
	        }
	    }
	    return false;
	}
	/*
	 * Scale down to original image height
	 * */
	
	private double calculateOriginalHeight(double imageHeightFromTracker, double originalImageHeight){
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double height = (int) screenSize.getHeight();
		double result;
		result = (imageHeightFromTracker/height)*originalImageHeight;
		return result;
	}
	/*Scale down to original image width
	 * */
	private double calculateOriginalWidth(double imageWidthFromTracker, double originalImageWidth){
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		double height = (int) screenSize.getWidth();
		double result;
		result = (imageWidthFromTracker/height)*originalImageWidth;
		return result;
	}
	@Override
	public void onGazeUpdate(GazeData gazeData){
		// TODO Auto-generated method stub

		
		if(imageIndex == 0 && firstTimeRunForTime){
			initialTimeStamp = gazeData.timeStamp;
			firstTimeRunForTime = false;
		}
		
		this.currentGazeData = gazeData;
		if(gazeData.isFixated){
		
			fixationCounter += 1;
			this.j.drawFixationsOnFrame(gazeData);
		
		}

		if(isFirstTime){
			//j.switchImage(currentImage, j.jF, j.jL, j.jP, gazeData, imageIndex);
			//isFirstTime = false;
		}
		
	
		


		double currentImageWidth = 0;
		double currentImageHeight = 0;
		
		
		String currentImagePath = testImagesPath.get(imageIndex);
	
		BufferedImage bimg;
		try {
			bimg = ImageIO.read(new File(currentImagePath));
			currentImageWidth = bimg.getWidth();
			currentImageHeight = bimg.getHeight();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		double lEyeCoordsY = Math.round(calculateOriginalHeight(gazeData.leftEye.smoothedCoordinates.y, currentImageHeight));
		double lEyeCoordsX = Math.round(calculateOriginalWidth(gazeData.leftEye.smoothedCoordinates.x, currentImageWidth));
		
		double rEyeCoordsY = Math.round(calculateOriginalHeight(gazeData.rightEye.smoothedCoordinates.y, currentImageHeight));
		double rEyeCoordsX = Math.round(calculateOriginalWidth(gazeData.rightEye.smoothedCoordinates.x, currentImageWidth));

		PrintWriter outputStreamForCurrentImage = null;
		int indexToCreatePathForFolder = j.testImagesPath.get(1).indexOf("\\");
		String pathToCreateFolderIn = j.testImagesPath.get(1).substring(0, indexToCreatePathForFolder);
		String finalPathForFolderCreation = pathToCreateFolderIn + "/testImagesOutputData";
		
	
		int indexToGetJustNameWithoutDot = imageList.get(imageIndex).indexOf(".");
		String fixationDataForCurrentImage = imageList.get(imageIndex).substring(0, indexToGetJustNameWithoutDot)+"_allCoords"+".txt";
		
		File currentFileToWriteIn = new File(fixationDataForCurrentImage);

	
		
		try {
		
			if(currentFileToWriteIn.exists() && !currentFileToWriteIn.isDirectory()){
				outputStreamForCurrentImage = new PrintWriter(new FileWriter(fixationDataForCurrentImage,true));
				outputStreamForCurrentImage.write("LEye coords ("+lEyeCoordsX +", "+lEyeCoordsY+")");
				outputStreamForCurrentImage.write(", REye coords (" + rEyeCoordsX + ", " +rEyeCoordsY+")");
				outputStreamForCurrentImage.write(", timeStamp: " + gazeData.timeStampString + "\n");
				outputStreamForCurrentImage.close();
			}
			else{
				outputStreamForCurrentImage = new PrintWriter(currentFileToWriteIn);
				outputStreamForCurrentImage.write("LEye coords ("+lEyeCoordsX+", "+lEyeCoordsY+")");
				outputStreamForCurrentImage.write(", REye coords (" + rEyeCoordsX + ", " +rEyeCoordsY+")");
				outputStreamForCurrentImage.write(", timeStamp: " + gazeData.timeStampString+"\n");
				outputStreamForCurrentImage.close();
			}

			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
		//int indexRem = imageList.get(imageIndex).indexOf(".");
		//String fileNameForRemoval = imageList.get(imageIndex).substring(0, indexRem)+"_allCoords"+".txt";
		//File fileRem = new File(fileNameForRemoval);
		if(e.getKeyCode() == KeyEvent.VK_Y){
			//	j.switchImage(image, jF, jL, jP, gazeData);
			
			
			String fileName = testImagesPath.get(imageIndex+1);
			if(trainingMode && fileName.endsWith("_withBD.jpg")){
		
				 try {
					BufferedImage img = ImageIO.read(new File(fileName));
					BufferedImage bi = img;
					Graphics2D ig2 = bi.createGraphics();
					ig2.setColor(Color.BLUE);
					if(currentGazeData.rightEye.smoothedCoordinates.x == 0){
						currentGazeData.rightEye.smoothedCoordinates.x=currentGazeData.leftEye.smoothedCoordinates.x;
					}
					if(currentGazeData.leftEye.smoothedCoordinates.x == 0){
						currentGazeData.leftEye.smoothedCoordinates.x = currentGazeData.rightEye.smoothedCoordinates.x;
					}
					if(currentGazeData.leftEye.smoothedCoordinates.x == 0 && currentGazeData.rightEye.smoothedCoordinates.x == 0){
						
					}
					if(currentGazeData.rightEye.smoothedCoordinates.y == 0){
						currentGazeData.rightEye.smoothedCoordinates.y=currentGazeData.leftEye.smoothedCoordinates.y;
					}
					if(currentGazeData.leftEye.smoothedCoordinates.y == 0){
						currentGazeData.leftEye.smoothedCoordinates.y = currentGazeData.rightEye.smoothedCoordinates.y;
					}
					int xCoord = (int) Math.round(calculateOriginalWidth((currentGazeData.rightEye.smoothedCoordinates.x+currentGazeData.leftEye.smoothedCoordinates.x)/2,img.getWidth()));
					int yCoord = (int) Math.round(calculateOriginalHeight((currentGazeData.rightEye.smoothedCoordinates.y+currentGazeData.leftEye.smoothedCoordinates.y)/2,img.getHeight()));
					ig2.fillOval(xCoord, yCoord, 9, 9);
					//ig2.dispose();
					ImageIO.write(bi, "jpg", new File(fileName));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		
				int indexToGetJustNameWithoutDot = imageList.get(imageIndex).indexOf(".");
				String fixationDataForCurrentImage = imageList.get(imageIndex).substring(0, indexToGetJustNameWithoutDot)+"_fixatedData"+".txt";
				String spaceFileName = fixationDataForCurrentImage;//"space"+imageIndex+".txt";
				
				
				PointerInfo a = MouseInfo.getPointerInfo();
				Point b = a.getLocation();
				int mouseX = (int) b.getX();
				int mouseY = (int) b.getY();
			
				int Low = -8;
				int High = 8;
			
				double currentImageWidth = 0;
				double currentImageHeight = 0;
			
				String currentImagePath = testImagesPath.get(imageIndex);
				
				BufferedImage bimg;
				try {
					bimg = ImageIO.read(new File(currentImagePath));
					currentImageWidth = bimg.getWidth();
					currentImageHeight = bimg.getHeight();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				// Testing these with relation to gazecoordinates. Accuracy of gaze
				double mouseWidthInPicTerm = Math.round(calculateOriginalWidth(mouseX, currentImageWidth));
				double mouseHeightInPicTerm = Math.round(calculateOriginalHeight(mouseY, currentImageHeight));
						
				double gazeLeftEyeX = Math.round(currentGazeData.leftEye.smoothedCoordinates.x);
				gazeLeftEyeX = Math.round(calculateOriginalWidth(gazeLeftEyeX, currentImageWidth));
				double gazeLeftEyeY = Math.round(currentGazeData.leftEye.smoothedCoordinates.y);
				gazeLeftEyeY = Math.round(calculateOriginalWidth(gazeLeftEyeY, currentImageHeight));
				double gazeRightEyeX = Math.round(currentGazeData.rightEye.smoothedCoordinates.x);
				gazeRightEyeX = Math.round(calculateOriginalWidth(gazeRightEyeX, currentImageWidth));
				double gazeRightEyeY = Math.round(currentGazeData.rightEye.smoothedCoordinates.y);
				gazeRightEyeY = Math.round(calculateOriginalWidth(gazeRightEyeY, currentImageHeight));
				
				PrintWriter outputStreamForCurrentImage = null;
				File currentFileToWriteIn = new File(spaceFileName);
				if(currentFileToWriteIn.exists() && !currentFileToWriteIn.isDirectory()){
					try {
						outputStreamForCurrentImage = new PrintWriter(new FileWriter(spaceFileName,true));
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				
					finalTimeStamp = currentGazeData.timeStamp;
					double timeTake = (finalTimeStamp - initialTimeStamp);
					double timeTaken = timeTake/1000.0;
					averageTime = averageTime + timeTaken;
				
					outputStreamForCurrentImage.write("LEye coords ("+gazeLeftEyeX+", "+gazeLeftEyeY+")");
					outputStreamForCurrentImage.write(", REye coords (" + gazeRightEyeX+", "+gazeRightEyeY+")");
					outputStreamForCurrentImage.write(", timeStamp: " + currentGazeData.timeStampString);
					//outputStreamForCurrentImage.write(", MousePosition ("+mouseWidthInPicTerm+", "+mouseHeightInPicTerm+")" +"\n");
					outputStreamForCurrentImage.write(", timeTaken: "+timeTaken+", totalFixationsTillNow: "+fixationCounter+"\n");
					outputStreamForCurrentImage.close();
				}
				else{
					try {
						outputStreamForCurrentImage = new PrintWriter(currentFileToWriteIn);
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					finalTimeStamp = currentGazeData.timeStamp;
					double timeTake = (finalTimeStamp - initialTimeStamp);
					double timeTaken = timeTake/1000.0;
					averageTime = averageTime+ timeTaken;
		
					outputStreamForCurrentImage.write("LEye coords ("+gazeLeftEyeX+", "+gazeLeftEyeY+")");
					outputStreamForCurrentImage.write(", REye coords (" + gazeRightEyeX+", "+gazeRightEyeY+")");
					outputStreamForCurrentImage.write(", timeStamp: " + currentGazeData.timeStampString);
					//outputStreamForCurrentImage.write(", MousePosition ("+mouseWidthInPicTerm+", "+mouseHeightInPicTerm+")" + "\n");
					outputStreamForCurrentImage.write(", timeTaken: "+timeTaken +", totalFixationsTillNow: "+fixationCounter+"\n");
					outputStreamForCurrentImage.close();
				}
				imageIndex++;
				
				startFromPreviousTime = null;
				previousImageIndexFName = "PreviousImageIndex.txt";
				previousImageIndex = new File(previousImageIndexFName);
				if(previousImageIndex.exists() && !previousImageIndex.isDirectory()){
					try {
						startFromPreviousTime = new PrintWriter(new FileWriter(previousImageIndex, false));
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						Date date = new Date();
						date = sdf.parse(sdf.format(new Date()));
						startFromPreviousTime.write(date.toString());
						startFromPreviousTime.write("\n"+imageIndex);
						startFromPreviousTime.close();
					} catch (IOException | ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
				else{
					try {
						startFromPreviousTime = new PrintWriter(new FileWriter(previousImageIndex, false));
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						Date date = new Date();
						date = sdf.parse(sdf.format(new Date()));
						startFromPreviousTime.write(date.toString());
						startFromPreviousTime.write("\n"+imageIndex);
						startFromPreviousTime.close();
					}  catch (IOException | ParseException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
				if(imageIndex %15 ==0 && imageIndex != 0 && !trainingMode){
					j.jF.dispose();
					System.exit(0);
				}
				try {
					initialTimeStamp = currentGazeData.timeStamp;
					j.switchImageTest(imageIndex);
					
				} catch (IOException e1) {

						e1.printStackTrace();
					}
				
		}
		
		if(e.getKeyCode() == KeyEvent.VK_SHIFT){
			Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
			BufferedImage capture;
			try {
				capture = new Robot().createScreenCapture(screenRect);
				ImageIO.write(capture, "bmp", new File("fixation")); 
			} catch (AWTException | IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
			
		}
		/*For training Mode
		 * */
		
		if(e.getKeyCode() == KeyEvent.VK_T){
			this.trainingMode = true;
		}
		/*Resume from previous state
		 * */
		if(e.getKeyCode() == KeyEvent.VK_R){
			// So that it gets recorded in the fixated file with a single point.

			//Set the image index from the previous one if the timestamp is the same
			File fileToReadIndexFrom = new File("PreviousImageIndex.txt");
			if(fileToReadIndexFrom.exists()){
				FileInputStream fstream = null;
				try {
					fstream = new FileInputStream("PreviousImageIndex.txt");
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
				String strLine;
				int lineCounter = 0;
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
				Date date = new Date();
				
				
				//Date dateWithoutTime = sdf.parse(sdf.format(new Date()));
				try {
					date = sdf.parse(sdf.format(new Date()));
					String currentDate = date.toString();
					
					while ((strLine = br.readLine()) != null)   {
						  // Print the content on the console
						String previousDate = strLine;
						if(lineCounter == 0){
						
							if(!currentDate.equals(previousDate)){
					
								break;
							}	
							
						}
						if(lineCounter == 1){
							imageIndex = Integer.parseInt(strLine);
							
							
							j.switchImageTest(imageIndex);
						}
						lineCounter++;
				
					}
				} catch (IOException | ParseException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	
			}
			else{
				
			}
		}
		if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
			j.jF.dispose();
			System.exit(0);
		}

		
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	

}
