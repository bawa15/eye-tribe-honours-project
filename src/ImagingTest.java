import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

import javax.imageio.ImageIO;

import com.theeyetribe.clientsdk.data.GazeData;

/*
 * This class contains functions to write text to an image which is used for making gaze plots
 * Used to resie image to full size, again used for gaze plots
 * And has a method copy which can copy an image from source to dest.(Still need to implement this part so that
 * it creates a new folder in the original location.
 * This class also contains the drawLine method. This method is basically going to be used for visualisation.
 * 
 * */

public class ImagingTest {

	/*    public static void main(String[] args) throws IOException {
	    	long time = System.currentTimeMillis();
	       //This part is just to resize the image
	        BufferedImage originalImage = ImageIO.read(new File("/Users/prateekbawa/Documents/JavaLatestWorkspace/EyeTribeTest/j.jpg"));//change path to where file is located
	        int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
	        
	        BufferedImage resizeImageJpg = resizeImage(originalImage, type, 1280, 800);
	        ImageIO.write(resizeImageJpg, "jpg", new File("/Users/prateekbawa/Documents/JavaLatestWorkspace/EyeTribeTest/j.jpg"));
	        //Writing on Image code starts here
	        String text = "HOW ARE YOUg!, HELLO HOW ARE";
	        File url = new File("/Users/prateekbawa/Documents/JavaLatestWorkspace/EyeTribeTest/j.jpg");
	        byte[] b = mergeImageAndText(url, text, new Point(500, 500));
	        FileOutputStream fos = new FileOutputStream("/Users/prateekbawa/Documents/JavaLatestWorkspace/EyeTribeTest/j.jpg");
	        fos.write(b);
	        fos.close();
	        System.out.println("Seconds=");
	        System.out.println(System.currentTimeMillis() - time); 
	        File url2 = new File("/Users/prateekbawa/Documents/JavaLatestWorkspace/EyeTribeTest/testImages/j2.jpg");
	        System.out.println("length="+url2.length()/1024);
	    }*/
	    public static byte[] mergeImageAndText(File url,
	            String text, Point textPosition) throws IOException {
	        BufferedImage im = ImageIO.read(url);
	        
	        Graphics2D g2 = im.createGraphics();
	        //g2.drawimag
	      //  g2.drawImage(im, 0, 0, 1280, 800, null);
	        g2.drawString(text, textPosition.x, textPosition.y);
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        ImageIO.write(im, "jpg", baos);
	        return baos.toByteArray();
	    }
	    public static byte[] mergeImageAndText(File url,
	            String text, GazeData textPosition) throws IOException {
	        BufferedImage im = ImageIO.read(url);
	        
	        Graphics2D g2 = im.createGraphics();
	        //g2.drawimag
	      //  g2.drawImage(im, 0, 0, 1280, 800, null);
	        g2.drawString(text, textPosition.smoothedCoordinates.x, textPosition.smoothedCoordinates.y);
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        ImageIO.write(im, "jpg", baos);
	        return baos.toByteArray();
	    }
	    /*This method is for visualisation. It basically draws arrows between previous fixation and the current fixation.
	     * */
	    
	    public static byte[] drawLineBetweenFixations(File url,
	        GazeData currentGazeData, GazeData previousGazeData, int d, int h) throws IOException {
	        BufferedImage im = ImageIO.read(url);
	        
	        Graphics2D g2 = im.createGraphics();
	        //g2.drawimag
	      //  g2.drawImage(im, 0, 0, 1280, 800, null);
	        int x1 = (int) previousGazeData.smoothedCoordinates.x;
	    	int y1 = (int) previousGazeData.smoothedCoordinates.y;
	    	int x2 = (int) currentGazeData.smoothedCoordinates.x;
	        int y2 = (int) currentGazeData.smoothedCoordinates.y;
	        
	    	int dx = x2 - x1, dy = y2 - y1;
	        double D = Math.sqrt(dx*dx + dy*dy);
	        double xm = D - d, xn = xm, ym = h, yn = -h, x;
	        double sin = dy/D, cos = dx/D;

	        x = xm*cos - ym*sin + x1;
	        ym = xm*sin + ym*cos + y1;
	        xm = x;

	        x = xn*cos - yn*sin + x1;
	        yn = xn*sin + yn*cos + y1;
	        xn = x;

	        int[] xpoints = {x2, (int) xm, (int) xn};
	        int[] ypoints = {y2, (int) ym, (int) yn};
	        //g2.drawString(text, textPosition.smoothedCoordinates.x, textPosition.smoothedCoordinates.y);
	        g2.drawLine(x1, y1, x2, y2);
	        g2.fillPolygon(xpoints, ypoints, 3);
	        ByteArrayOutputStream baos = new ByteArrayOutputStream();
	        ImageIO.write(im, "jpg", baos);
	        return baos.toByteArray();
	    }
	    public static BufferedImage resizeImage(BufferedImage originalImage, int type, int IMG_WIDTH, int IMG_HEIGHT) {
		    BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
		    Graphics2D g = resizedImage.createGraphics();
		    g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
		    g.dispose();
		    
		    return resizedImage;
		}
	    public void copy(File src, File dst) throws IOException {
	        InputStream in = new FileInputStream(src);
	        try {
	            OutputStream out = new FileOutputStream(dst);
	            try {
	                // Transfer bytes from in to out
	                byte[] buf = new byte[1024];
	                int len;
	                while ((len = in.read(buf)) > 0) {
	                    out.write(buf, 0, len);
	                }
	            } finally {
	                out.close();
	            }
	        } finally {
	            in.close();
	        }
	    }
	    /**
	      * Draw an arrow line betwwen two point 
	      * @param g the graphic component
	      * @param x1 x-position of first point
	      * @param y1 y-position of first point
	      * @param x2 x-position of second point
	      * @param y2 y-position of second point
	      * @param d  the width of the arrow
	      * @param h  the height of the arrow
	      */
	     public void drawArrowLine(Graphics g, GazeData currentData, GazeData previousData, int d, int h){
	    	int x1 = (int) previousData.smoothedCoordinates.x;
	    	int y1 = (int) previousData.smoothedCoordinates.y;
	    	int x2 = (int) currentData.smoothedCoordinates.x;
	        int y2 = (int) currentData.smoothedCoordinates.y;
	        
	    	int dx = x2 - x1, dy = y2 - y1;
	        double D = Math.sqrt(dx*dx + dy*dy);
	        double xm = D - d, xn = xm, ym = h, yn = -h, x;
	        double sin = dy/D, cos = dx/D;

	        x = xm*cos - ym*sin + x1;
	        ym = xm*sin + ym*cos + y1;
	        xm = x;

	        x = xn*cos - yn*sin + x1;
	        yn = xn*sin + yn*cos + y1;
	        xn = x;

	        int[] xpoints = {x2, (int) xm, (int) xn};
	        int[] ypoints = {y2, (int) ym, (int) yn};

	        g.drawLine(x1, y1, x2, y2);
	        g.fillPolygon(xpoints, ypoints, 3);
	     }
	     
	}


