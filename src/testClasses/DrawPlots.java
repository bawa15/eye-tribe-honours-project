package testClasses;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class DrawPlots {

	private JFrame jF;
	ImageIcon img;
	public void drawFrame(){
		jF.setTitle("Tutorial123");
		jF.setUndecorated(true);
		jF.setResizable(false);
		jF.setVisible(true);
		
		Toolkit tk = Toolkit.getDefaultToolkit();
		int xsize = (int)tk.getScreenSize().getWidth();
		int ysize = (int)tk.getScreenSize().getHeight();
		jF.setSize(xsize, ysize);
		
		jF.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	public void drawImage(){
		
	}
	@SuppressWarnings("serial")
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		final BufferedImage image = new BufferedImage(1280, 768,
	            BufferedImage.TYPE_INT_RGB);
	    JPanel canvas = new JPanel() {
	        @Override
	        protected void paintComponent(Graphics g) {
	            super.paintComponent(g);
	            g.drawImage(image, 0, 0, this);
	        }
	    };

	    JFrame frame = new JFrame();
	    frame.setLayout(new BorderLayout());   // <== make panel fill frame
	    frame.add(canvas, BorderLayout.CENTER);
	    frame.setSize(500, 500);
	    frame.setVisible(true);

	    // do you drawing somewhere else, maybe a different thread
	    Graphics g = image.getGraphics();
	    g.setColor(Color.red);
	    for (int x = 0; x < 100; x += 5) {
	        for (int y = 0; y < 100; y += 5) {
	            g.drawRect(x, y, 1, 1);
	        }
	    }
	    g.dispose();
	    canvas.repaint();
	}

}
