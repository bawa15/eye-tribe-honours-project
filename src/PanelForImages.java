
import java.awt.Font;
import java.awt.Graphics;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


import javax.imageio.ImageIO;

import javax.swing.JPanel;

import com.theeyetribe.clientsdk.data.GazeData;

/*Panel used for images. This creates the panel to which the image is atached.*/
@SuppressWarnings("serial")
public class PanelForImages extends JPanel{

	private BufferedImage image;
	private GazeData gazeData;
//	private BufferedImage currentImage;
	public PanelForImages(File image) throws IOException{
		//this.image = image;
		//URL resource = getClass().getResource("so2.jpg");
		this.image = ImageIO.read(image);
		//this.gazeData = gazeData;
		
	}
//	public void currentImageCheck(BufferedImage image){
//		this.currentImage = this.image;
//	}
	public void drawGaze(GazeData gazeData){
		this.gazeData = gazeData;
	}
	@Override
	public void paintComponent(Graphics g){
		//super.paint(g);
		//super.paintComponents(g);
		super.paintComponent(g);
		//g.drawImage(image, 3, 4, this);
		
		g.drawImage(image, 0, 0, getWidth(), getHeight(), this);
		
		
		//currentImageCheck(image);
		//g.drawString("HELLOOOOOOOOOOOOOOOOOOOOOOOO", 400, 400);
		Font font = new Font("Verdana", Font.BOLD, 16);
		g.setFont(font);
		
		if(gazeData != null){
			//g.drawString("X",(int)gazeData.smoothedCoordinates.x,(int) gazeData.smoothedCoordinates.y);

		}
		//g.drawString("X1(300,400)", 300, 400);
	///	g.drawString("X2(200,200)", 200, 200);
		//g.drawString("X3", 500, 550);
		g.dispose();
		//g.drawString("TEST", (int)gazeData.smoothedCoordinates.x,(int) gazeData.smoothedCoordinates.y);
		//repaint();
		
	}
//	@Override
//	public void paint(Graphics g){
//		super.paint(g);
//		g.drawString("XXXXX",(int)gazeData.smoothedCoordinates.x,(int) gazeData.smoothedCoordinates.y);
//	g.dispose();
//		
//	}
	
}
