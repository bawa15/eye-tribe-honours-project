import javax.xml.parsers.DocumentBuilderFactory;
	import javax.xml.parsers.DocumentBuilder;
	import org.w3c.dom.Document;
	import org.w3c.dom.NodeList;
	import org.w3c.dom.Node;
	import org.w3c.dom.Element;
	import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;

/*Class used to extract centres from fixation data*/
public class ComputeAccuracy {

	public static void main(String argv[]) {
		File folder = new File("/Users/prateek/Documents/workspace/EyeTribeTest/Experiments/Annotator1/Bus02.03.2017");
		File[] files = folder.listFiles();
		for(File file: files){
			try {
				 // File fXmlFile = new File(file);
				File fXmlFile = new File("/Users/prateek/Documents/workspace/EyeTribeTest/FilesWithExactCentres/bus/000132.xml.txt");
				if(file.isFile() && file.getName().endsWith(".xml")){
					fXmlFile = file;
				}
				String objectName;
				int centreX;
				int centreY;
				String fileName=fXmlFile.getName();
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(fXmlFile);

				  //optional, but recommended
				  //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
				doc.getDocumentElement().normalize();

				System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

				NodeList nList = doc.getElementsByTagName("object");

				System.out.println("----------------------------");

				for (int temp = 0; temp < nList.getLength(); temp++) {

					Node nNode = nList.item(temp);

					System.out.println("\nCurrent Element :" + nNode.getNodeName());

					if (nNode.getNodeType() == Node.ELEMENT_NODE) {
						PrintWriter outputStreamForCurrentImage = null;
						
						
						File currentFileToWriteIn = new File(fXmlFile.getName()+".txt");
						  //System.out.println("filen="+currentFileToWriteIn.getName());
						Element eElement = (Element) nNode;
						if(eElement.getElementsByTagName("name").item(0).getTextContent().equals("bus")){
							  objectName = eElement.getElementsByTagName("name").item(0).getTextContent();
							  int xmin = Integer.parseInt(eElement.getElementsByTagName("xmin").item(0).getTextContent());
							  int xmax = Integer.parseInt(eElement.getElementsByTagName("xmax").item(0).getTextContent());
							  centreX = (xmin+xmax)/2;
							  int ymin = Integer.parseInt(eElement.getElementsByTagName("ymin").item(0).getTextContent());
							  int ymax = Integer.parseInt(eElement.getElementsByTagName("ymax").item(0).getTextContent());
							  centreY = (ymin+ymax)/2; 
							  System.out.println("Filename: "+fXmlFile.getName());
							  System.out.println("name: "+objectName);
							  System.out.println("centreX: "+centreX);
							  System.out.println("centreY: "+centreY);
							  if(currentFileToWriteIn.exists() && !currentFileToWriteIn.isDirectory()){
									try {
										outputStreamForCurrentImage = new PrintWriter(new FileWriter(fXmlFile,true));
									} catch (IOException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
									
									outputStreamForCurrentImage.write(fXmlFile.getName());
									outputStreamForCurrentImage.write(" centreX: "+centreX);
									outputStreamForCurrentImage.write(" centreY: "+centreY);
									outputStreamForCurrentImage.write(" bus");
									outputStreamForCurrentImage.close();
								}
								else{
									try {
										outputStreamForCurrentImage = new PrintWriter(currentFileToWriteIn);
										outputStreamForCurrentImage.write(fXmlFile.getName());
										outputStreamForCurrentImage.write(" centreX: "+centreX);
										outputStreamForCurrentImage.write(" centreY: "+centreY);
										outputStreamForCurrentImage.write(" bus");
										outputStreamForCurrentImage.close();
									} catch (FileNotFoundException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
							  
								}
						//  File fixatedFolder = new File("/Users/prateek/Documents/workspace/EyeTribeTest/Experiments/Annotator2/Cow05.03.2017");
						  //File[] fixatedFiles = fixatedFolder.listFiles();
//						  for(File fixatedList : fixatedFiles){
//							  int averageOfLeftRightX;
//							  int averageOfLeftRightY;
//							  if(fixatedList.getName().equals(file.getName())){
//								  
//							  }
//						  }
//						  System.out.println("name : " + eElement.getElementsByTagName("name").item(0).getTextContent());
//						  System.out.println("xmin : " + eElement.getElementsByTagName("xmin").item(0).getTextContent());
//						  System.out.println("ymin : " + eElement.getElementsByTagName("ymin").item(0).getTextContent());
//						  System.out.println("xmax : " + eElement.getElementsByTagName("xmax").item(0).getTextContent());
//						  System.out.println("ymax : " + eElement.getElementsByTagName("ymax").item(0).getTextContent());

					  }
				  }
			  }} catch (Exception e) {
				  e.printStackTrace();
			  }
			  
		  }
	  
}
}

