import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;


/*Class used for analysis to calcualte error Distance
 * */
public class CalculateAccuracyAndWrite {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		File folder = new File("/Users/prateek/Documents/workspace/EyeTribeTest/Experiments/Annotator2/DiningTable05.03.2017");
		File[] files = folder.listFiles();
		
		for(File fil: files){
			BufferedReader br = null;
			br = new BufferedReader(new FileReader(fil));
			String line;
			int lX=0;
			int lY=0;
			int rY =0;
			int rX = 0;
			int xCoord=0;
			int yCoord=0;
		
			System.out.println(fil.getName().substring(0,6));
			if(fil.isFile() && fil.getName().endsWith("_fixatedData.txt")){
				while((line = br.readLine()) != null){
					
					String[] commas = line.split(" ");
				//	
					if(commas[2].length() == 7){
						lX = Integer.parseInt(commas[2].substring(1, 4));
						//rX = Integer.parseInt(commas[6].substring(1, 4));
						//System.out.println(lX);
					
					}
					if(commas[2].length() == 6){
						lX = Integer.parseInt(commas[2].substring(1, 3));
						//System.out.println(lX);
				
					}
					if(commas[6].length() == 7){
					
						rX = Integer.parseInt(commas[6].substring(1, 4));
					
						//System.out.println(rX);
					}
					if(commas[6].length() == 6){
						
						rX = Integer.parseInt(commas[6].substring(1, 3));
					
						//System.out.println(rX);
					}
					
					if(commas[3].length() == 6){
						 lY = Integer.parseInt(commas[3].substring(0, 1));
						// System.out.println(lY);

					}
					if(commas[3].length() == 7){
						 lY = Integer.parseInt(commas[3].substring(0, 3));
						// System.out.println(lY);

					}
					if(commas[7].length() == 7){
						
						rY = Integer.parseInt(commas[7].substring(0,3));
					
						//System.out.println(rY);
					}
					if(commas[7].length() == 6){
						
						rY = Integer.parseInt(commas[7].substring(0, 2));
					
						//System.out.println(rY);
					}
					xCoord = (rX+lY)/2;
					yCoord = (rY+lY)/2;
					File exactCenters = new File("/Users/prateek/Documents/workspace/EyeTribeTest/FilesWithExactCentres/diningtable");
					File[] exactCenterFiles = exactCenters.listFiles();
					for(File exactCen : exactCenterFiles){
						//System.out.println(exactCen.getName().substring(0,6));
						String exactCentFileName = exactCen.getName().substring(0,6);
						String myCentFileName = fil.getName().substring(0,6);
						if(exactCen.isFile() && exactCentFileName.equals(myCentFileName)){
							BufferedReader exactCents = null;
							exactCents = new BufferedReader(new FileReader(exactCen));
							//System.out.println("ever go in here");
							while((line = exactCents.readLine()) != null){
								String[] words = line.split(" ");
								int exactCentX = Integer.parseInt(words[2]);
								int exactCentY = Integer.parseInt(words[4]);
								double xSquare = Math.pow(exactCentX-xCoord, 2);
								double ySquare = Math.pow(exactCentY-yCoord, 2);
								double errorDist = Math.sqrt(xSquare+ySquare);
								System.out.println("error="+errorDist);
								File fileNames = new File("fileNames.txt");
								File Accuracies = new File("accuracies.txt");
								PrintWriter fileName = null;
								PrintWriter accuracies = null;
								if(fileNames.exists() && !fileNames.isDirectory()){
									try {
										fileName = new PrintWriter(new FileWriter(fileNames,true));
										accuracies = new PrintWriter(new FileWriter(Accuracies,true));
									} catch (IOException e1) {
										// TODO Auto-generated catch block
										e1.printStackTrace();
									}
								
									fileName.write(exactCentFileName + "\n");
									accuracies.write((int) errorDist + "\n");
									accuracies.close();
									fileName.close();
								}
								else{
									fileName = new PrintWriter(new FileWriter(fileNames));
									accuracies = new PrintWriter(new FileWriter(Accuracies));
									fileName.write(exactCentFileName + "\n");
									accuracies.write((int) errorDist + "\n");
									accuracies.close();
									fileName.close();
								}
									
								}
							break;
						}
					}
					//System.out.println(commas[3]);
//					if(commas[3].length() == 5){
//						System.out.println(commas[3]);
//					//	rX = Integer.parseInt(commas[3].substring(0, 2));
//						//System.out.println(rX);
//					}
				
					
					
					//System.out.println(time[5]);
			//		totalTimePerImage +=  Double.parseDouble(time[2]);
					
				}
			}
			
		}
	}

}
