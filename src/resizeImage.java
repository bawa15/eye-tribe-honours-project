import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/*Used to scale up the image to full screen. */
public class resizeImage {
	/*public static void main(String[] args) {

	    try {
	    	Toolkit tk = Toolkit.getDefaultToolkit();
			int xsize = (int)tk.getScreenSize().getWidth();
			int ysize = (int)tk.getScreenSize().getHeight();
	        BufferedImage originalImage = ImageIO.read(new File("/Users/prateekbawa/Documents/JavaLatestWorkspace/EyeTribeTest/testImages/Unknown.jpg"));//change path to where file is located
	        int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();

	        BufferedImage resizeImageJpg = resizeImage(originalImage, type, xsize, ysize);
	        ImageIO.write(resizeImageJpg, "jpg", new File("/Users/prateekbawa/Documents/JavaLatestWorkspace/EyeTribeTest/testImages/UnknownResized.jpg")); //change path where you want it saved

	    } catch (IOException e) {
	        System.out.println(e.getMessage());
	    }

	}*/

	@SuppressWarnings("unused")
	private static BufferedImage resizeImage(BufferedImage originalImage, int type, int IMG_WIDTH, int IMG_HEIGHT) {
	    BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
	    Graphics2D g = resizedImage.createGraphics();
	    g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
	    g.dispose();

	    return resizedImage;
	}
}
