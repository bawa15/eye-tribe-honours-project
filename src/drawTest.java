import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;

/*used during initial development to test various functions*/
public class drawTest extends JPanel {

   public void paintComponent(Graphics g) {
      Image img = null;
      try {
    	  img = createImageWithText();
      } catch (IOException e) {
		// TODO Auto-generated catch block
    	  e.printStackTrace();
      }
     // g.drawImage(img, 40,20,this);
	
      g.drawImage(img, 0, 0, 1080, 700, this);
   }

   private Image createImageWithText() throws IOException{
      //BufferedImage bufferedImage = new BufferedImage(1280, 800,BufferedImage.TYPE_INT_RGB);
   //   BufferedImage bufferedImage = new BufferedImage()
	   BufferedImage bufferedImage = ImageIO.read(getClass().getResource("Unknown.jpg"));
      Graphics g = bufferedImage.getGraphics();
     
      g.drawString("www.tutorialspoint.com", 20,20);
      g.drawString("www.tutorialspoint.com", 20,40);
      g.drawString("www.tutorialspoint.com", 20,60);
      g.drawString("www.tutorialspoint.com", 200,100);
     
      g.drawString("POINT WAS HEEEEEEEEEEEEEEEEEEEEEE", 33,5);
  
    	  
      return bufferedImage;
   }
   
   public static void main(String[] args) {
      JFrame frame = new JFrame();
      Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
      double width = screenSize.getWidth();
      double height = screenSize.getHeight();
      
      frame.getContentPane().add(new drawTest());

      frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     // frame.setSize(200, 200);
      
      frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
      System.out.println(height + " " + width); 
      frame.setVisible(true);
   }
}