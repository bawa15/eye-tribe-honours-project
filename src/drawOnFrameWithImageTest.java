import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


/*Test class used initially during development.
 * */
@SuppressWarnings("serial")
public class drawOnFrameWithImageTest extends JFrame implements KeyListener{
	
	private ArrayList<String> testImagesPath = new ArrayList<String>();
	private ArrayList<String> testImagesList = new ArrayList<String>();
	private int ImageIndex = 0;
	public void putImagesInArrayList() throws InvocationTargetException, InterruptedException{
		testImagesFolder testImages = new testImagesFolder();
		this.testImagesPath = testImages.getFilePaths();
		this.testImagesList = testImages.getImageList(testImagesPath);
		//drawImageOnFrame(testImagesList.get(0));
	//	jP.addKeyListener(this);
		//jP.setFocusable(true);
	}
	public drawOnFrameWithImageTest(String s){
	
		setTitle("Tutorial");
		
	}
	public drawOnFrameWithImageTest(){
		ImageIcon img;
		JPanel jP = new JPanel();
		JLabel jL = new JLabel();
		setTitle("Tutorial");
		setUndecorated(true);
		setResizable(false);
		setVisible(true);
		
		//setExtendedState(JFrame.MAXIMIZED_BOTH); 
		Toolkit tk = Toolkit.getDefaultToolkit();
		int xsize = (int)tk.getScreenSize().getWidth();
		int ysize = (int)tk.getScreenSize().getHeight();
		setSize(xsize, ysize);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		img = new ImageIcon("Unknown.jpg");
		
		Image imag = img.getImage();
		BufferedImage bi = new BufferedImage(1280, 800, BufferedImage.TYPE_INT_ARGB);
		
		Graphics g = bi.createGraphics();
		g.drawImage(imag, 0, 0, 1280, 800, null);
		g.setColor(Color.GREEN);
		//659.5999, 133.6668
		g.drawString("EHLLO", 1200, 700);
	//	1180.0414, 717.9911
		ImageIcon newIcon = new ImageIcon(bi);
		//jL.setSize(500, 500);
		jL.setIcon(newIcon);
		add(jL);
		jP.add(jL);
		add(jP);
		
		validate();
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//drawOnFrameWithImageTest t = new drawOnFrameWithImageTest();
	}
//	public static void main(String[] args) {
//		// TODO Auto-generated method stub
//		drawOnFrameWithImageTest t = new drawOnFrameWithImageTest();
//		t.putImagesInArrayList();
//	}
	
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		if(e.getKeyCode() == KeyEvent.VK_RIGHT){
			ImageIndex++;
			System.out.println(ImageIndex);
			System.out.println(testImagesList.get(ImageIndex));
			//drawImageOnFrame(testImagesList.get(ImageIndex));
		}
		
	}

}
