import java.awt.Color;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Image;

import java.awt.event.WindowAdapter;
import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.theeyetribe.clientsdk.data.GazeData;

/*This class contains the frame. It will be used to get and display the images.
 * */
public class justFrame{
	protected BufferedImage bi;
	protected JFrame jF = new JFrame();
	protected JPanel jP = new JPanel();
	protected JLabel jL = new JLabel();
	protected PanelForImages panelForImages;
	protected ImageIcon img;
	protected ArrayList<String> testImagesPath = new ArrayList<String>();
	protected ArrayList<String> testImagesList = new ArrayList<String>();
	protected ArrayList<String> testImagesAbsoluteFilePaths = new ArrayList<String>();

	//private NextImageUsingArrows changeImage = new NextImageUsingArrows();

	private int imageIndex = 0;
	private String currentFile;

//	public justFrame(){
		//jF.addKeyListener(changeImage);
//	}
	public void putImagesInArrayList() throws InvocationTargetException, InterruptedException{
		
		testImagesFolder testImages = new testImagesFolder();
		this.testImagesPath = testImages.getFilePaths();
		this.testImagesList = testImages.getImageList(testImagesPath);
		this.testImagesAbsoluteFilePaths = testImages.getAbsoluteFilePaths();
		this.currentFile = testImagesAbsoluteFilePaths.get(imageIndex);
		try {
			this.panelForImages = new PanelForImages(new File(currentFile));
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	//	jP.addKeyListener(this);
		//jP.setFocusable(true);
	}
	protected ArrayList<String> getImagesList(){
		return this.testImagesList;
	}
//	protected void switchImage(){
//		System.out.println("Does this get called"+testImagesList.get(1));
//		img = new ImageIcon(testImagesList.get(1));
//		
//		img.getImage().flush();
//		jL.setIcon(img);
//		jP.add(jL);
//		jF.validate();
//	}

	public void setCurrentImageIndex(int imageIndex){
		this.imageIndex = imageIndex;
	}
/*	public void showFrame(){
		
		System.out.println("Is this getting called everytiem="+getImagesList());
		
		//if(testImagesList != null){
		//jF.setTitle("Tutorial123");
		jF.setUndecorated(true);
		jF.setResizable(false);
		
		
		Toolkit tk = Toolkit.getDefaultToolkit();
		int xsize = (int)tk.getScreenSize().getWidth();
		int ysize = (int)tk.getScreenSize().getHeight();
		jF.setSize(xsize, ysize);
		
		//jF.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//setSize(800, 800);
		//jF.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		//setUndecorated(true);
		//jF.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//String testImage = testImagesList.get(0);
		//System.out.println(testImage);
		//BufferedImage newImage = ImageIO.read(new File(testImage);
		//img = new ImageIcon(this.getClass().getResource(testImage));
		//Image scaleImage = img.getImage().getScaledInstance(28, 28, Image.SCALE_DEFAULT);
		if(testImagesList != null){
			img = new ImageIcon(testImagesList.get(0));
		}
		Image imag = img.getImage();
		bi = new BufferedImage(1280, 800, BufferedImage.TYPE_INT_ARGB);
		Graphics g = bi.createGraphics();
		
		g.drawImage(imag, 0, 0, 1280, 800, null);
		//currentImageIcon = img;
		ImageIcon newIcon = new ImageIcon(bi);
		jL.setIcon(newIcon);
		//jL.setIcon(img);
		//jL.setSize(800, 8000);
		//jF.add(jL);		//Removed because of stackoverflow comment
		jP.add(jL);
		jF.add(jP);
		//jP.add(jL);
		//jL.add(ballPanel);
		//jP.add(ballPanel);
		//jP.add(ballPanel);
		//jF.add(jP);
		
		//setVisible(true);
		//add(ballPanel);
		//GazeListener g = new GazeListener();
		//add(g);
		//jF.validate();
		
		jF.setVisible(true);
		//System.out.println(f.exists());
		//}
		//else{
		//	System.exit(0);
		//}
		jF.validate();
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gs = ge.getDefaultScreenDevice();
		gs.setFullScreenWindow(jF);
		
	}*/
	public void showFrame(int imageIndex) throws IOException{
		System.out.println("Does it come here");
		if(testImagesList != null){
			//img = new ImageIcon(testImagesList.get(0));
			//BufferedImage img1 = ImageIO.read(new File(testImagesList.get(0)));
		}
		
		//jF.setUndecorated(true);
		//System.out.println(testImagesList.get(0));
		System.out.println(testImagesPath);
		System.out.println("AbsolutePath=");
		System.out.println(testImagesPath.get(0));
		//System.out.println(testImagesA);
		//jF.add(new PanelForImages((new File(testImagesAbsoluteFilePaths.get(8)))));
		jF.add(panelForImages);
		
//	    jF.add(new Component() {
//	      //  BufferedImage img = ImageIO.read(file);
//	    	
//	    	//BufferedImage img1 = ImageIO.read(new File(testImagesPath.get(1)));
//	    	
//	    	BufferedImage img2 = ImageIO.read(new File(testImagesAbsoluteFilePaths.get(6)));
//	      //  BufferedImage bi = new BufferedImage(1280, 800, BufferedImage.TYPE_INT_ARGB);
//	    	BufferedImage img3 = ImageIO.read(new File(testImagesAbsoluteFilePaths.get(imageIndex))); 
//	    	@Override
//	        public void paint(Graphics g) {
//	            super.paint(g);
//	          //  g.drawImage(img2, 0, 0, getWidth(), getHeight(), this);
//	            g.drawImage(img3, 0, 0, getWidth(), getHeight(), this);
//	            
//	            
//	        }
//	    });
	    
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice gs = ge.getDefaultScreenDevice();
		jF.addWindowListener(new WindowAdapter() {
		    
		});
		
		
		//if(gs.isFullScreenSupported()){
			gs.setFullScreenWindow(jF);
		
		
		
		jF.validate();
	}
	public void drawFixationsOnFrame(GazeData gazeData){
	
		Graphics g = panelForImages.getGraphics();
		panelForImages.drawGaze(gazeData);
		panelForImages.paintComponent(g);
		//Font font = new Font("Verdana", Font.BOLD, 16);
		//g.setFont(font);
		//g.setColor(Color.Blue);
		//g.drawString("X", (int)gazeData.smoothedCoordinates.x, (int)gazeData.smoothedCoordinates.y);

	   // g.dispose();
		//jLL.setBounds(, 3, 3);
		
		///jLL.setLocation((int)gazeData.smoothedCoordinates.x,(int) gazeData.smoothedCoordinates.y);
		//this.panelForImages.add(jLL);
		//this.panelForImages.add(new DrawFixations(300, 500));
		
		//jF.remove(drawFixations);
		//this.drawFixations.removeAll();
	//	this.drawFixations = new DrawFixations(gazeData.smoothedCoordinates.x, gazeData.smoothedCoordinates.y);
		//this.drawFixations.repaint();
		//jF.add(this.drawFixations);
		
		//Graphics g = this.panelForImages.getGraphics();
		
		//this.panelForImages.add(new DrawFixations(500,200));
		//g.drawString("TEsting 123", (int)gazeData.smoothedCoordinates.x, (int)gazeData.smoothedCoordinates.y);
		//this.drawFixations = new DrawFixations(400, 500);
		
		//jF.getContentPane().add(drawFixations);
		
//		bi = new BufferedImage(1280, 800, BufferedImage.TYPE_INT_ARGB);
//		Font font = new Font("Verdana", Font.BOLD, 16);
//		
//		Graphics g = bi.createGraphics();
//		g.setFont(font);
//		String calibrateText = "O";
//		g.drawString(calibrateText, (int)gazeData.smoothedCoordinates.x, (int)gazeData.smoothedCoordinates.y);
//		ImageIcon newIcon = new ImageIcon(bi);
//		
//		jL.setIcon(newIcon);
//		jP.add(jL);
//		jF.validate();
		//g.dispose();
		
		jF.revalidate();
	}
	public void switchImageTest(int imageIndex ) throws IOException{
		
		//jF.add(new PanelForImages(new File(testImagesAbsoluteFilePaths.get(imageIndex))));
		jF.remove(panelForImages);
		panelForImages.removeAll();
		File currentImage = new File(testImagesAbsoluteFilePaths.get(imageIndex));
		this.panelForImages = new PanelForImages(currentImage);
		jF.add(panelForImages);
//		jF.add(new Component(){
//			BufferedImage img = ImageIO.read(new File(testImagesAbsoluteFilePaths.get(imageIndex)));
//			@Override
//			public void paint(Graphics g){
//				super.paint(g);
//				g.drawImage(img, 0, 0, getWidth(), getHeight(), this);
//				//repaint();
//				g.dispose();
//			}
//		});
		//GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		//GraphicsDevice gs = ge.getDefaultScreenDevice();
		//gs.setFullScreenWindow(jF);
		//jF.repaint();
		jF.validate();
	}
	
	/*This function is to switch images when arrow key is pressed. Currently, it also has the code to draw
	 * the coordinate which moves.*/
	public void switchImage(ImageIcon image, JFrame jF, JLabel jL, JPanel jP, GazeData gazeData, int imageIndex){
		System.out.println("x="+gazeData.smoothedCoordinates.x);
		Image imag = image.getImage();	
		BufferedImage bi = new BufferedImage(1280, 800, BufferedImage.TYPE_INT_ARGB);
		Graphics g = bi.getGraphics();
		//Graphics g = bi.createGraphics();
		g.drawImage(imag, 0, 0, 1280, 800, null);
	
		g.setColor(Color.black);

		Font font = new Font("Verdana", Font.BOLD, 16);
		g.setFont(font);
		
	
		//g.drawString(calibrateText, x, y);
		if(imageIndex == 0){
			g.drawString("X1", 400, 500);
		}
		else if(imageIndex == 1){
			g.drawString("X1", 650, 300);
		}
		else if(imageIndex == 2){
			g.drawString("X1", 550, 200);
		}
		else if(imageIndex == 3){
			g.drawString("X1", 700, 700);
		}
		else if(imageIndex == 4){
			g.drawString("X1", 300, 300);
		}
		//g.drawString("X2", 387, 498);
		//g.drawString( "X0",353, 476);
		//g.drawString("X3", 384, 542);
		ImageIcon newIcon = new ImageIcon(bi);
		
		jL.setIcon(newIcon);
		jP.add(jL);
		jF.validate();
	
	}
	
	
	public void moveFixation(JPanel jP, BufferedImage bi, GazeData gazeData){
		
		Graphics g = bi.createGraphics();
		g.setColor(Color.black);
		int x = (int) gazeData.rawCoordinates.x;
		int y = (int) gazeData.rawCoordinates.y;
		Font font = new Font("Verdana", Font.BOLD, 16);
		g.setFont(font);
		g.drawString("EHLLO", x, y);
		ImageIcon newIcon = new ImageIcon(bi);
		jL.setIcon(newIcon);
		jP.add(jL);
		jF.validate();
	}
	public void drawFixations(GazeData gazeData ){
		Graphics g = this.bi.createGraphics();
		g.setColor(Color.black);
		int x = (int) gazeData.rawCoordinates.x;
		int y = (int) gazeData.rawCoordinates.y;
		Font font = new Font("Verdana", Font.BOLD, 16);
		g.setFont(font);
		g.drawString("EHLLO", x, y);
		
		
	}
	
	
}
