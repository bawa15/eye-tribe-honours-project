
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;

import javax.swing.ImageIcon;

import com.theeyetribe.clientsdk.GazeManager;

import com.theeyetribe.clientsdk.GazeManagerCore.ApiVersion;
import com.theeyetribe.clientsdk.GazeManagerCore.ClientMode;

/*Main function where everything is setup and gazelistener is called to start the process.*/
public class Main {


	protected ImageIcon currentImageIcon;
	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws IOException, InvocationTargetException, InterruptedException {
		// TODO Auto-generated method stub
		//Uncomment this if you get a blackscreen in the beginnning itself
		//System.setProperty("sun.java2d.d3d", "false");
		 
		final GazeManager gm = GazeManager.getInstance();
		@SuppressWarnings("unused")
		boolean succ = gm.activate(ApiVersion.VERSION_1_0, ClientMode.PUSH);
		//System.out.println("Suucess="+succ);
		final GazeListener gazeListener = new GazeListener();
		//gazeListener.imageList = j.getImagesList();
		//gazeListener.imageList = j.getImagesList();
	    gm.addGazeListener(gazeListener);
	 
		    //TODO: Do awesome gaze control wizardry
	    
        Runtime.getRuntime().addShutdownHook(new Thread(){
        	@Override
	        public void run(){
        		
         		gm.removeGazeListener(gazeListener);
        		gm.deactivate();
        	}
        });
        
		
	}

}
