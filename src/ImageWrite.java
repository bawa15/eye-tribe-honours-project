import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


/*used for analysis to calculate center using xmax/min, ymax/min*/
public class ImageWrite {

	static public void main(String args[]) throws Exception {
	    try {
	     
//xC=(xmin+xmax)/2 and yC=(ymin+ymax)/2.
	    	//height of the box is ymax-ymin and the width is xmax-xmin. 
	      // TYPE_INT_ARGB specifies the image format: 8-bit RGBA packed
	      // into integer pixels
	    	/*<bndbox>
			<xmin>134</xmin>
			<ymin>30</ymin>
			<xmax>437</xmax>
			<ymax>375</ymax>
		</bndbox>*/
	      int xmin = 134;
	      int ymin = 30;
	      int xmax = 437;
	      int ymax = 375;
	      int width = xmax - xmin;
	      int height = ymax -ymin;
	      int Cx = (xmin + xmax)/2;
	      int Cy = (ymax + ymin) / 2;
	  
	      String fileName = "/Users/prateek/Documents/workspace/EyeTribeTest/ValidationClasses/004312";
	      String fN = fileName + ".jpg";
	      BufferedImage img = ImageIO.read(new File(fN));
	    
	      BufferedImage bi = img;
	    
	      Graphics2D ig2 = bi.createGraphics();
	     // ig2.drawImage(img, 0,0, null);
	      ig2.setColor(Color.GREEN);
	      ig2.drawRect(xmin, ymin, width, height);
	     // ig2.drawRect(x, y, width, height);
	      ig2.setColor(Color.GREEN);
	      ig2.fillOval(Cx, Cy, 6, 6);
	    //  ig2.fillOval(x, y, width, height);
	     // ig2.drawString("X", 146, 306);
	      String fileName2 = fileName + "_withBD.jpg";
	      ImageIO.write(bi, "jpg", new File(fileName2));
	      
	      
	    } catch (IOException ie) {
	      ie.printStackTrace();
	    }

	  }
}
